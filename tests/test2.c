#include "test.h"

void test2(FILE* f, void const* heap) {
    debug("-> TEST #2 STARTED\n");

    void* alloc1 = _malloc(42000);
    if (alloc1 == NULL)
        debug("_malloc(42000) failed\n");

    void* alloc2 = _malloc(3077);
    if (alloc2 == NULL)
        debug("_malloc(3077) failed\n");

    debug_heap(f, heap);

    _free(alloc1);
    _free(alloc2);

    debug("-> TEST #2 FINISHED\n\n");
}