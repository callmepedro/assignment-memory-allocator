#include "test.h"

void test1(FILE* f, void const* heap) {
    debug("-> TEST #1 STARTED\n");

    void* alloc1 = _malloc(100);
    if (alloc1 == NULL)
        debug("_malloc(100) failed\n");

    void* alloc2 = _malloc(42);
    if (alloc2 == NULL)
        debug("_malloc(42) failed\n");

    void* alloc3 = _malloc(1024);
    if (alloc1 == NULL)
        debug("_malloc(1024) failed\n");

    debug_heap(f, heap);
    
    _free(alloc1);
    debug_heap(f, heap);

    _free(alloc2);
    _free(alloc3);
    debug_heap(f, heap);

    void* alloc4 = _malloc(1000);
    if (alloc4 == NULL)
        debug("_malloc(1000) failed\n");

    debug_heap(f, heap);
    _free(alloc4);

    debug("-> TEST #1 FINIFHED\n\n");
}