#include "test.h"

size_t heap_size(void const* heap) {
  struct block_header* block = (struct block_header*)heap;
  size_t sz = 0;
  while(block){
    sz += size_from_capacity(block->capacity).bytes;
    block = block->next;
  }
  return sz;
}

void test4(FILE* f, void const* heap) {
    debug("-> TEST #4 STARTED\n");

	size_t sz = heap_size(heap);
	void* heap_end = (void*)((uint8_t*)HEAP_START + sz);
	void* alloc_trash_after_heap = map_pages(heap_end, 5000, MAP_FIXED_NOREPLACE);
	debug("Allocated trash after end of the heap: %p\n", alloc_trash_after_heap);
    debug_heap(f, heap);

    void* alloc_above_heap = _malloc(sz+1234);
    debug_heap(f, heap);

    _free(alloc_above_heap);
    debug_heap(f, heap);

    _free(alloc_trash_after_heap);
    debug("-> TEST #4 FINIFHED\n\n");
}