#ifndef TEST_H_
#define TEST_H_

#include <stdarg.h>
#define _DEFAULT_SOURCE

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug(const char* fmt, ... );

void test1(FILE* f, void const* heap);
void test2(FILE* f, void const* heap);
void test3(FILE* f, void const* heap);
void test4(FILE* f, void const* heap);

#endif
