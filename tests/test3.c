#include "test.h"

void test3(FILE* f, void const* heap) {
    debug("-> TEST #3 STARTED\n");

    void* alloc1 = _malloc(1);
    if (alloc1 == NULL)
        debug("_malloc(1) failed\n");

    debug_heap(f, heap);

    void* alloc2 = _malloc(100000);
    if (alloc2 == NULL)
        debug("_malloc(100000) failed\n");

    debug_heap(f, heap);

    _free(alloc1);
    _free(alloc2);

    debug("-> TEST #3 FINIFHED\n\n");
}