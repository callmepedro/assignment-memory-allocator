CFLAGS=--std=c17 -Wall -pedantic -Isrc/ -ggdb -Wextra -Werror -DDEBUG
BUILDDIR=build
TESTDIR=tests
SRCDIR=src
CC=gcc

.PHONY: all build clean

all: $(BUILDDIR)/mem.o $(BUILDDIR)/util.o $(BUILDDIR)/mem_debug.o $(BUILDDIR)/main.o $(BUILDDIR)/test1.o $(BUILDDIR)/test2.o $(BUILDDIR)/test3.o $(BUILDDIR)/test4.o
	$(CC) -o $(BUILDDIR)/main $^

build:
	mkdir -p $(BUILDDIR)

$(BUILDDIR)/%.o: $(TESTDIR)/%.c build
	$(CC) -c $(CFLAGS) $< -o $@

$(BUILDDIR)/%.o: $(SRCDIR)/%.c build
	$(CC) -c $(CFLAGS) $< -o $@

run: $(BUILDDIR)/main
	./$(BUILDDIR)/main

clean:
	rm -rf $(BUILDDIR)

