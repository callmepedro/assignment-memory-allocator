#include "../tests/test.h"
#include "mem.h"

#define HEAP_INITIAL_SIZE 42000

int main() {
    void* heap = heap_init(HEAP_INITIAL_SIZE);

    test1(stdout, heap);
    test2(stdout, heap);
    test3(stdout, heap);
    test4(stdout, heap);

    return 0;
}